FROM openjdk:11-jdk-slim as build
WORKDIR /app
COPY . /app
RUN ./gradlew bootJar

FROM adoptopenjdk:11-jre-hotspot
COPY --from=build /app/build/libs/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
