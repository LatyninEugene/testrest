package com.latynin.rest.elastic;


import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Data
@FieldNameConstants
public class Category {
    @NonNull private String id;
    @Nullable private Category parent;
    @NonNull private String content;
}
