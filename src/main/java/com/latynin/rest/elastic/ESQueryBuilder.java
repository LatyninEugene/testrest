package com.latynin.rest.elastic;

import lombok.val;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;

public class ESQueryBuilder {

    private BoolQueryBuilder root;

    public ESQueryBuilder(BoolQueryBuilder root) {
        this.root = root;
    }

    public ESQueryBuilder addTextMatch(String text) {
        addTextMatch(text, false, false);
        return this;
    }

    public ESQueryBuilder addTextMatch(String text, boolean specific, boolean isFuzzyPossible) {
        val query = new MultiMatchQueryBuilder(
                text,
                Product.Fields.name
        );
        query.field(Product.Fields.description, 0.75f);
        query.field(Product.Fields.category + "." + Category.Fields.content, 0.75f);
        query.type(MultiMatchQueryBuilder.Type.MOST_FIELDS);
        if (isFuzzyPossible) {
            if (text.length() > 12) {
                query.fuzziness(Fuzziness.TWO);
            } else if (text.length() > 7) {
                query.fuzziness(Fuzziness.ONE);
            }
        }
        if (specific) {
            query.operator(Operator.AND);
        }
        root.must(query);
        return this;
    }

    public ESQueryBuilder addCategoryFilter(String categoryId) {
        root.filter(new TermQueryBuilder(Product.Fields.category + "." + Category.Fields.id, categoryId));
        return this;
    }

    public QueryBuilder build() {
        return root;
    }

}
