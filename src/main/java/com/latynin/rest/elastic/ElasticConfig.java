package com.latynin.rest.elastic;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.lang.NonNull;

@Configuration
public class ElasticConfig extends AbstractElasticsearchConfiguration {

    private final String url;
    private final long connectTimeout;
    private final long socketTimeout;

    @Autowired
    public ElasticConfig(
            @NonNull @Value("${db.elastic.url}") String url,
            @Value("${db.elastic.timeout.connect}") long connectTimeout,
            @Value("${db.elastic.timeout.socket}") long socketTimeout
    ) {
        this.url = url;
        this.connectTimeout = connectTimeout;
        this.socketTimeout = socketTimeout;
    }

    @NonNull
    @Override
    public RestHighLevelClient elasticsearchClient() {
        ClientConfiguration config = ClientConfiguration.builder()
                .connectedTo(url)
                .withConnectTimeout(this.connectTimeout)
                .withSocketTimeout(this.socketTimeout)
                .build();
        return RestClients.create(config).rest();
    }

}
