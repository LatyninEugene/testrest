package com.latynin.rest.controllers;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.latynin.rest.elastic.Product;
import com.latynin.rest.models.dto.DcCatalog;
import com.latynin.rest.models.request.ProductSearchRequest;
import com.latynin.rest.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @Operation(summary = "Получает xml по переданной ссылки и импортирует данные в elastic")
    @RequestMapping(path = "/importFromUrl", method = {RequestMethod.POST, RequestMethod.GET})
    private ResponseEntity<Object> importFromUrlProduct(@RequestParam("url") String strUrl) throws IOException {
        URL url = new URL(strUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        val content = (InputStream) con.getContent();
        return ResponseEntity.ok(service.importProduct(new XmlMapper().readValue(content, DcCatalog.class)));
    }

    @Operation(summary = "Передаётся xml данные, которые впоследствии импортируются в elastic")
    @RequestMapping(path = "/import", method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE)
    private ResponseEntity<Object> importProduct(@RequestBody DcCatalog data) {
        return ResponseEntity.ok(service.importProduct(data));
    }

    @Operation(summary = "Возвращает список продуктов, соответствующих тексту запроса и фильтрам")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the book",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProductResult.class)) }) })
    @RequestMapping(path = "/find", method = RequestMethod.POST)
    private ResponseEntity<Object> findProduct(@RequestBody ProductSearchRequest data) {
        return ResponseEntity.ok(service.findProducts(data));
    }

}

class ProductResult extends PageImpl<Product>{

    public ProductResult(List<Product> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public ProductResult(List<Product> content) {
        super(content);
    }
}
