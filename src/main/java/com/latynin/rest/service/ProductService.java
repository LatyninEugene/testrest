package com.latynin.rest.service;

import com.latynin.rest.elastic.Category;
import com.latynin.rest.elastic.ESQueryBuilder;
import com.latynin.rest.elastic.Product;
import com.latynin.rest.elastic.repository.ProductRepository;
import com.latynin.rest.models.dto.DcCatalog;
import com.latynin.rest.models.request.ProductSearchRequest;
import lombok.val;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository repository;
    private final ElasticsearchOperations esOperations;

    public ProductService(
            ProductRepository repository,
            @Qualifier("elasticsearchOperations") ElasticsearchOperations esOperations) {
        this.repository = repository;
        this.esOperations = esOperations;
    }

    public Boolean importProduct(DcCatalog data) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        TemporalAccessor temporalAccessor = formatter.parse(data.getLast_update());
        LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        Instant lastUpdate = Instant.from(zonedDateTime);
        val categories = data.getDelivery_service().getCategories()
                .getCategory().stream()
                .map(cat -> new Category(cat.getId(), cat.getContent()))
                .collect(Collectors.toMap(Category::getId, cat -> cat));
        val products = data.getDelivery_service().getProducts()
                .getProduct().stream()
                .map(product -> new Product(
                        product.getId(),
                        categories.get(product.getCategory_id()),
                        product.getPrice(),
                        product.getName(),
                        product.getDescription(),
                        product.getPicture(),
                        lastUpdate
                )).collect(Collectors.toList());
        repository.saveAll(products);
        return true;
    }

    public PageImpl<Product> findProducts(ProductSearchRequest data) {
        val page = data.getPage();
        val limit = data.getLimit();
        if (page < 0 || limit < 1 || limit * page >= 10_000) {
            return new PageImpl<Product>(List.of());
        }
        val queryBuilder = new ESQueryBuilder(new BoolQueryBuilder());
        val filter = data.getFilter();
        if (filter == null) {
            queryBuilder.addTextMatch(data.getText());
        } else {
            queryBuilder.addTextMatch(data.getText(), filter.isSpecific(), filter.isFuzzyPossible());
            if (filter.getCategoryId() != null) {
                queryBuilder.addCategoryFilter(filter.getCategoryId());
            }
        }
        val nsqb = new NativeSearchQueryBuilder();
        nsqb.withQuery(queryBuilder.build());
        nsqb.withPageable(PageRequest.of(page, limit));
        val result = esOperations.search(nsqb.build(), Product.class)
                .getSearchHits().stream().map(SearchHit::getContent).collect(Collectors.toList());
        return new PageImpl<Product>(result);
    }
}
