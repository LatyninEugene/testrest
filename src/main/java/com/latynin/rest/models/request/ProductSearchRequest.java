package com.latynin.rest.models.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Data
@NoArgsConstructor
public class ProductSearchRequest {
    @NonNull private String text;
    private int page = 0;
    private int limit = 10;
    @Nullable private ProductSearchFilter filter;
}
