package com.latynin.rest.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
public class ProductSearchFilter {
    @Nullable
    private String categoryId;
    private boolean specific = false;
    private boolean isFuzzyPossible = false;
}