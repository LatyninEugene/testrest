package com.latynin.rest.models.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.*;
import lombok.Data;

@Data
@JacksonXmlRootElement
public class Category {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    @JacksonXmlProperty(isAttribute = true)
    private String parentId;
    @JacksonXmlText
    private String content;
}
