package com.latynin.rest.models.dto;

import lombok.Data;

@Data
public class Delivery_service {
    Categories categories;
    Products products;
}