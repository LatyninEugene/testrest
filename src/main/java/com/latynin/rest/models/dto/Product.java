package com.latynin.rest.models.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JacksonXmlRootElement
public class Product {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    @JacksonXmlProperty
    private String category_id;
    @JacksonXmlProperty
    private String price;
    @JacksonXmlProperty
    private String name;
    @JacksonXmlProperty
    private String description;
    @JacksonXmlProperty
    private String picture;
}
