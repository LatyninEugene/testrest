package com.latynin.rest.models.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

@Data
@JacksonXmlRootElement
public class DcCatalog {
    Delivery_service delivery_service;
    private String last_update;
}
